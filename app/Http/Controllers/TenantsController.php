<?php

namespace App\Http\Controllers;

use App\Models\Tenant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TenantsController extends Controller
{
    

    public function store()
    {
        $this->validate(request(),[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'subdomain' => ['required','string','unique:domains,domain']        
        ]);
        
        $tenant = Tenant::create();

        $tenant->domains()->create([
            'domain' => request('subdomain'),
        ]);

        $user = '';
        $tenant->run(function() use(&$user){
            $user = User::create([
                'name' => request('name'),
                'email' => request('email'),
                'password' => Hash::make(request('password')),
            ]);
        });
        $redirectUrl = '/dashboard';
        $token = tenancy()->impersonate($tenant, $user->id, $redirectUrl);

        return redirect(tenant_route($tenant->domains()->first()->domain  . '.' . request()->getHttpHost(), 'impersonate',[
            'token' => $token
        ]));

    }
}

